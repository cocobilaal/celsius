package sheridan;

public class Celsius {

	public static int fromFahrenheit(int fahrenheit) {
		int c = (fahrenheit - 32) * 5 / 9;

		return c;
	}
}
