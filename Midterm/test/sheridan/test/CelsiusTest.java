// Bilaal Rashid

package sheridan.test;

import static org.junit.Assert.*;

import org.junit.Test;

import sheridan.Celsius;

public class CelsiusTest {

	@Test
	public void testFromFahrenheitRegular() {
		int fahrenheit = Celsius.fromFahrenheit(15);
		assertTrue("Invalid Fahrenheit", fahrenheit == 15);
	}

	@Test (expected = NumberFormatException.class)
	public void testFromFahrenheitException() {
		int fahrenheit = Celsius.fromFahrenheit(-35);
		assertFalse("Invalid Fahrenheit", fahrenheit == -35);
	}
	
	@Test
	public void testFromFahrenheitBoundaryIn() {
		int fahrenheit = Celsius.fromFahrenheit(99);
		assertTrue("Invalid Fahrenheit", fahrenheit == 99);
	}
	
	@Test
	public void testFromFahrenheitBoundaryOut() {
		int fahrenheit = Celsius.fromFahrenheit(100);
		assertFalse("Invalid Fahrenheit", fahrenheit == 100);
	}

}